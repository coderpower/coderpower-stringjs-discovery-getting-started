### Getting started with String.js

This first introduction will cover the following functions:

```javascript
    S('Hello_world').camelize()
    S('left center right').between('left', 'right')
    S(foobar).s
```

For more information, please refer to the documentation: http://stringjs.com.
