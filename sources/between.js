var S = require('string');

module.exports = function between(foo) {
    foo.string = "This is a random string";
    foo.left = "This";
    foo.right = "string";

    foo.stringBetween = S(foo.string).between(foo.left, foo.right).s;
    console.log('What is between: ' + foo.stringBetween);

    return foo;
};
