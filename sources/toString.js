var S = require('string');

module.exports = function toString() {
    var foo = S('hello').s;
    console.log('foo to string', foo);

    var array = S(['a, b']).s;
    console.log('array to string', array);

    var object = S({hello: 'Coderpower'}).s;
    console.log('object to string', object);

    return object;
};
