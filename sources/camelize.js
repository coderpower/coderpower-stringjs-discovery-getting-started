var S = require('string');

module.exports = function camelize(foo) {
    foo.string = "this-is-a-random-slug";

    foo.stringCamelized = S(foo.string).camelize().s;
    console.log('foo.string is camelized', foo.stringCamelized);

    return foo;
};
