var S = require('string');
var chai = require('chai');
var expect = chai.expect;

// Sources
var camelize = require('../sources/camelize');
var between = require('../sources/between');
var toString = require('../sources/toString');

describe('Getting started with String.js.', function() {
    it('should call the camelize function', function(done) {
        var foo = {
            string: 'this-is-a-random-slug'
        };
        foo = camelize(foo);
        expect(foo.stringCamelized).to.exist;
        expect(foo.stringCamelized).to.eql(S(foo.string).camelize().s);
        done();
    });
    it('should call the between function', function(done) {
        var foo = {
            string: 'This is a random string',
            left: 'This',
            right: 'string'
        };
        foo = between(foo);
        expect(foo.stringBetween).to.exist;
        expect(foo.stringBetween).to.eql(S(foo.string).between(foo.left, foo.right).s);
        done();
    });
    it('should return a string', function(done) {
        var result = toString();
        expect(result).to.be.a('string');
        done();
    });
});
